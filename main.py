#Import of system for commandline
import sys
# Import of the module Fonctions
from Fonctions import *

# Definition of the commandline parameters
x = int(sys.argv[1])
y = int(sys.argv[3])
operator = sys.argv[2]

# statements for operations according to the operator
if operator == "+":
    a = addition(x,y)
    print(a)
if operator == "/":
    d = division(x,y)
    print(d)
if operator == "*":
    m = multiplication(x,y)
    print(m)
if operator == "-":
    s = soustraction(x,y)
    print(s)


